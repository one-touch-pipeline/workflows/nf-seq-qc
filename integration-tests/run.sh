#!/bin/bash

set -ue
set -o pipefail

profile="${1:?No profile given (docker, mamba, apptainer)}"
outDir="${2:?No outDir given}"
# By default only leave empty, which means take the workflow's default.
maxCpus="${3:-}"

mkdir -p "$outDir"

testsDir="$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")"
srcDir="$testsDir/.."

TEST_TOTAL=0
TEST_ERRORS=0

assertEqual() {
  local first="${1:?No first number given}"
  local second="${2:?No second number given}"
  local message="${3:?No message given}"
  TEST_TOTAL=$((TEST_TOTAL + 1))
  if [[ "$first" == "$second" ]]; then
    echo "Success: $message: $first == $second" >>/dev/stderr
  else
    echo "Failure: $message: $first != $second" >>/dev/stderr
    TEST_ERRORS=$((TEST_ERRORS + 1))
  fi
}

assertNotEqual() {
  local first="${1:?No first number given}"
  local second="${2:?No second number given}"
  local message="${3:?No message given}"
  TEST_TOTAL=$((TEST_TOTAL + 1))
  if [[ "$first" != "$second" ]]; then
    echo "Success: $message: $first != $second" >>/dev/stderr
  else
    echo "Failure: $message: $first == $second" >>/dev/stderr
    TEST_ERRORS=$((TEST_ERRORS + 1))
  fi
}

assertDirExists() {
  local path="${1:?No directory given}"
  test -d "$path"
  assertEqual "$?" 0 "Directory: $path"
}

assertFileExists() {
  local path="${1:?No file given}"
  test -f "$path"
  assertEqual "$?" 0 "File: $path"
}

testFinished() {
  echo "" >>/dev/stderr
  echo "$TEST_ERRORS of $TEST_TOTAL tests failed." >>/dev/stderr
  if [[ $TEST_ERRORS -gt 0 ]]; then
    exit 1
  else
    exit 0
  fi
}

# Run Nextflow with multiple different input formats. One point is that they all have the same
# seqBase, but that should not result in filename collisions. The other point is that they all
# should be processed correctly.
tmpDir="$(mktemp -d -p "$outDir" -t "multiple-input-files.XXXXXX")"

seqBase="run1_gerald_D1VCPACXX_1_R1.sorted"
seqPath="$testsDir/seqs/$seqBase"
declare -a seqs=(
  "$seqPath.fastq"
  "$seqPath.fastq.bz2"
  "$seqPath.fastq.gz"
  "$seqPath.fastq.tar"
  "$seqPath.fastq.tar.bz2"
  "$seqPath.fastq.tar.gz"
  "$seqPath.fastq.zip"
  "$seqPath.bam"
  "$seqPath.sam.gz"
  "$seqPath.cram"
)

set +e

nextflow run "$srcDir/main.nf" \
  -profile "test,$profile" \
  ${maxCpus:+--maxCpus=$maxCpus} \
  --input="$(echo "${seqs[@]}" | tr ' ' ',')" \
  --outputDir="$tmpDir"
assertEqual "$?" 0 "workflow runs with multiple input files"

for seq in "${seqs[@]}"; do
  seqOutDir="$tmpDir/$(basename "$seq")_reports"
  assertDirExists "$seqOutDir"
  assertFileExists "$seqOutDir/${seqBase}_fastqc.html"
  assertFileExists "$seqOutDir/${seqBase}_fastqc.zip"
done

nextflow run "$srcDir/main.nf" \
  -profile "test,$profile" \
  ${maxCpus:+--maxCpus=$maxCpus} \
  --input="/a/file.fastq,/b/file.fastq" \
  --outputDir="$tmpDir"
assertNotEqual "$?" 0 "recognize inputs reducing to the same output directory"


testFinished
