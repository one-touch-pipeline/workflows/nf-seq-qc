The following people and institutions have a copyright in parts or the whole of the nf-seq-qc workflow:
- Copyright © 2021  Philip Reiner Kensche
- Copyright © 2021  Valentin Schneider-Lunitz
- Copyright © 2021  German Cancer Research Center (Deutsches Krebsforschungszentrum, DKFZ)
- Copyright © 2021  Berlin Institute of Health (BIH)
