/*
 * Copyright (c) 2021. German Cancer Research Center (DKFZ).
 *
 * Distributed under the MIT License.
 * https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/-/blob/initial-version/LICENSE.txt
 *
 * Author: WESkit Team & OTP Authors
 */

package otp.nfseqqc


import spock.lang.Specification


class SuffixAnalysisResultTest extends Specification {
    def "Create"() {
        setup:
        Optional<SuffixAnalysisResult> sar =
                SuffixAnalysisResult.from(new File("/path/to/some.fastq.gz"))

        expect:
        sar.present
    }

    def "Parse variants"(filename, basename, suffix, check, decompression) {
        setup:
        SuffixAnalysisResult sar =
                SuffixAnalysisResult.from(new File(filename)).get()

        expect:
        sar.basename == basename
        sar.suffix == suffix
        sar.archiveTestCommand == check
        sar.decompressionCommand == decompression

        where:
        filename | basename | suffix | check | decompression
        "/path/to/some.fastq.gz"       | "some" | ".fastq.gz"       | "true" | "gunzip -c '$filename'"
        "/path/to/some.sam.bz2"        | "some" | ".sam.bz2"        | "true" | "bunzip2 -c '$filename'"
        "/path/to/some.bam"            | "some" | ".bam"            | "true" | "cat '$filename'"
        "/path/to/some.cram"           | "some" | ".cram"           | "true" | "samtools view -b '$filename'"
        "/path/to/some.gz"             | "some" | ".gz"             | "true" | "gunzip -c '$filename'"
        "/path/to/some.bz2"            | "some" | ".bz2"            | "true" | "bunzip2 -c '$filename'"
        "/path/to/some.csfastq.tar.gz" | "some" | ".csfastq.tar.gz" | "tar -tzf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOzf '$filename'"
        "/path/to/some.fq.tgz"         | "some" | ".fq.tgz"         | "tar -tzf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOzf '$filename'"
        "/path/to/some.sam.tar.bz2"    | "some" | ".sam.tar.bz2"    | "tar -tjf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOjf '$filename'"
        "/path/to/some.txt.tbz2"       | "some" | ".txt.tbz2"       | "tar -tjf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOjf '$filename'"
        "/path/to/some.txt.zip"        | "some" | ".txt.zip"        | "zipinfo -l '$filename' | [[ \$(wc -l) -eq 1 ]]" | "unzip -p '$filename'"
        "/path/to/some.tgz"            | "some" | ".tgz"            | "tar -tzf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOzf '$filename'"
        "/path/to/some.tar.bz2"        | "some" | ".tar.bz2"        | "tar -tjf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOjf '$filename'"
        "/path/to/some.tar"            | "some" | ".tar"            | "tar -tf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOf '$filename'"
        "/path/to/some.tbz2"           | "some" | ".tbz2"           | "tar -tjf '$filename' | [[ \$(wc -l) -eq 1 ]]" | "tar -xOjf '$filename'"
        "/path/to/some.zip"            | "some" | ".zip"            | "zipinfo -l '$filename' | [[ \$(wc -l) -eq 1 ]]" | "unzip -p '$filename'"
    }
}
