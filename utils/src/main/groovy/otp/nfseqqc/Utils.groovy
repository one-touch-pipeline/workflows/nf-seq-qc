/*
 * Copyright (c) 2021. German Cancer Research Center (DKFZ).
 *
 * Distributed under the MIT License.
 * https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/-/blob/initial-version/LICENSE.txt
 *
 * Author: WESkit Team & OTP Authors
 */

package otp.nfseqqc

import groovy.transform.CompileStatic

@CompileStatic
class Utils {

    /** Check whether parameters are correct (names and values)
     *
     * @param parameters
     * @param allowedParameters
     */
    static Set<String> collectUnknownParameters(Map<String, String> parameters,
                                         List<String> allowedParameters) {
        return parameters.
                keySet().
                grep { String name ->
                    !name.contains('-')
                    // Nextflow creates hyphenated versions of camel-cased parameters.
                }.
                minus(allowedParameters)
    }

    static Map<String,List<String>> collectClashingInputs(List<String> inputs) {
        return inputs.
                groupBy {new File(it).name }.
                findAll {  pair ->
                    pair.value.size() > 1
                }
    }

    static String parameterSummary(String name,
                                   List<String> allowedParams,
                                   Map<String, String> currentParams,
                                   Integer width = 50) {
        width = Math.max(width, name.size() + 4)
        return """
               |${(["="] * width).join("")}
               |${String.format("= %-${width - 4}s =", name)}
               |${(["="] * width).join("")}
               |${allowedParams.collect { "$it = ${currentParams.get(it)}" }.join("\n")}
               |""".stripMargin()
    }

}
