/*
 * Copyright (c) 2021. German Cancer Research Center (DKFZ).
 *
 * Distributed under the MIT License.
 * https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/-/blob/initial-version/LICENSE.txt
 *
 * Author: WESkit Team & OTP Authors
 */

package otp.nfseqqc

import groovy.transform.CompileStatic

import java.util.regex.Matcher

@CompileStatic
class SuffixAnalysisResult {

    /* Independent dimensions of information retrievable from filenames. */

    private enum Archiver {
        NONE, TAR, ZIP
    }

    private enum Compressor {
        NONE, GZ, BZ2, ZIP
    }

    private enum Format {
        FASTQ,
        BAM,
        SAM,
        CRAM
    }

    /** Parse basename, suffix, Archiver, Compressor, and Format from filename. */
    static private class FilenameInfo {
        final String filename
        final String basename
        final String suffix
        final Archiver archiver
        final Compressor compressor
        final Format format

        private FilenameInfo(String filename,
                             String basename,
                             String suffix,
                             Archiver archiver,
                             Compressor compressor,
                             Format format) {
            this.filename = filename
            this.basename = basename
            this.suffix = suffix
            this.archiver = archiver
            this.compressor = compressor
            this.format = format
        }

        String toString() {
            return "FilenameInfo(filename=$filename, basename=$basename, suffix=$suffix, archiver=$archiver, compressor=$compressor, format=$format)"

        }

        static Format formatFrom(String formatString) {
            Format format
            if (["txt", "fq", "fastq", "csfastq", "", null].contains(formatString))
                format = Format.FASTQ
            else if (formatString == "bam")
                format = Format.BAM
            else if (formatString == "sam")
                format = Format.SAM
            else if (formatString == "cram")
                format = Format.CRAM
            else
                throw new RuntimeException("Ooops! This should not happen! format=$formatString")

            return format
        }

        static Archiver archiverFrom(String compressorArchiverMatch) {
            Archiver archiver
            if (["tar", "tar.gz", "tgz", "tar.bz2", "tbz2"].contains(compressorArchiverMatch))
                archiver = Archiver.TAR
            else if (compressorArchiverMatch == "zip")
                archiver = Archiver.ZIP
            else
                archiver = Archiver.NONE

            return archiver
        }

        static Compressor compressorFrom(String compressorArchiverMatch) {
            Compressor compressor
            if (["tar.gz", "tgz", "gz"].contains(compressorArchiverMatch))
                compressor = Compressor.GZ
            else if (["tar.bz2", "tbz2", "bz2"].contains(compressorArchiverMatch))
                compressor = Compressor.BZ2
            else if (compressorArchiverMatch == "zip")
                compressor = Compressor.ZIP
            else if (["tar", null].contains(compressorArchiverMatch))
                compressor = Compressor.NONE
            else
                throw new RuntimeException("Ooops! This should not happen! compressor=$compressorArchiverMatch")
            return compressor
        }

        private static final String formatPat = 'fq|txt|fastq|csfastq|sam|bam|cram'
        private static final String compressorArchivePat = 'tar|tar\\.gz|tar\\.bz2|tgz|tbz2|gz|bz2|zip'

        static Optional<FilenameInfo> from(String filename) {
            Matcher matcher = filename =~ /^(.+?)((?:\.($formatPat))?(?:\.($compressorArchivePat))?)$/
            if (!matcher) {
                return Optional.empty()
            } else {
                String basenameMatch = matcher.group(1)
                String suffix = matcher.group(2)
                String formatNameMatch = matcher.group(3)
                String compressorArchiveMatch = matcher.group(4)

                return Optional.of(new FilenameInfo(
                        filename,
                        basenameMatch,
                        suffix,
                        archiverFrom(compressorArchiveMatch),
                        compressorFrom(compressorArchiveMatch),
                        formatFrom(formatNameMatch)))
            }

        }

    }


    final String basename
    final String suffix
    final String archiveTestCommand
    final String decompressionCommand
    final String fastqcFormatName

    private SuffixAnalysisResult(String basename, String suffix,
                                 String archiveTestCommand, String decompressionCommand,
                                 String fastqcFormatName) {
        this.basename = basename
        this.suffix = suffix
        this.archiveTestCommand = archiveTestCommand
        this.decompressionCommand = decompressionCommand
        this.fastqcFormatName = fastqcFormatName
    }

    static Optional<SuffixAnalysisResult> from(File file) {
        final String exactlyOneLine = '[[ $(wc -l) -eq 1 ]]'

        return FilenameInfo.from(file.name).map({ match ->
            String archiveTestCommand
            String decompressionCommand
            Map<Format, String> fastqcFormatNames = [
                (Format.CRAM): "bam",
                (Format.BAM): "bam",
                (Format.FASTQ): "fastq",
                (Format.SAM): "sam"
            ]
            String fastqcFormatName = fastqcFormatNames[match.format]
            if (match.archiver == Archiver.TAR) {

                if (match.compressor == Compressor.NONE) {
                    archiveTestCommand = "tar -tf '${file.path}' | $exactlyOneLine"
                    decompressionCommand = "tar -xOf '${file.path}'"

                } else if (match.compressor == Compressor.GZ) {
                    archiveTestCommand = "tar -tzf '${file.path}' | $exactlyOneLine"
                    decompressionCommand = "tar -xOzf '${file.path}'"

                } else if (match.compressor == Compressor.BZ2) {
                    archiveTestCommand = "tar -tjf '${file.path}' | $exactlyOneLine"
                    decompressionCommand = "tar -xOjf '${file.path}'"

                } else {
                    throw new RuntimeException("Unkown tar compression: $match")
                }

            } else if (match.archiver == Archiver.NONE) {
                archiveTestCommand = "true"

                if (       match.compressor == Compressor.NONE
                        && match.format == Format.CRAM) {
                    decompressionCommand = "samtools view -b '${file.path}'"

                } else if (match.compressor == Compressor.NONE) {
                    decompressionCommand = "cat '${file.path}'"

                } else if (match.compressor == Compressor.GZ) {
                    decompressionCommand = "gunzip -c '${file.path}'"

                } else if (match.compressor == Compressor.BZ2) {
                    decompressionCommand = "bunzip2 -c '${file.path}'"

                } else {
                    throw new RuntimeException("Unkown compression or format: $match")
                }

            } else if (match.archiver == Archiver.ZIP && match.compressor == Compressor.ZIP) {
                archiveTestCommand = "zipinfo -l '${file.path}' | $exactlyOneLine"
                decompressionCommand = "unzip -p '${file.path}'"

            } else {
                throw new RuntimeException("No commands to process file! $match")
            }

            new SuffixAnalysisResult(
                    match.basename,
                    match.suffix,
                    archiveTestCommand,
                    decompressionCommand,
                    fastqcFormatName)
        })
    }
}

