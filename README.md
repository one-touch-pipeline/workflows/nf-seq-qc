[![pipeline status](https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/badges/initial-version/pipeline.svg)](https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/-/commits/initial-version)

# nf-seq-qc

A simple "workflow" to obtain basic quality control measures for one or multiple FASTQs. Currently, it does just a wrapper for [fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) with some code to allow input of various compressed formats.

> NOTE: This workflow is for research-use only (RUO)!

## Setup

The workflow uses some Groovy code that compiles into a JAR file. This way the code can be better tested. The following command will compile this code, test it, and install it in the `$repoDir/lib/` directory.

```bash
cd $repoDir
./gradlew jar
```

Nextflow will automatically find the JAR file and use it for the execution of the workflow.

By default, the workflow will use our Docker container from the GitLab Container Registry.
Also, if you want to use Apptainer (e.g. because Docker is not available in your environment), Nextflow will download the container and convert it into a Apptainer format.

## Configuration

 * `input`: Comma-separated list of FASTQs. The filetype is recognized for each file independently based on its suffixes.
   * `.fastq`, `.fq`, `.csfastq`, `.txt`, `.sam`: Uncompressed sequence data in FASTQ or SAM (=TAM)
   * `.tar`, `.tar.gz`, `.tar.bz2`, `.tbz2`, `.tgz`, `.zip`: A single, uncompressed sequence file in compressed or uncompressed archive. May be combined with any of the uncompressed sequence format suffixes (above).
   * `.gz`, `.bz2`: Compressed sequence data in FASTQ or SAM format. May be combined with any of the uncompressed sequence format suffixes (above).
   * `.bam`: BAM format (binary SAM). No combination with archive or additional compression is supported.
   * `.cram`: CRAM format. No combination with archive or additional compression is supported.
 * `outputDir`: Output directory. Each input file gets a directory in the output directory that collects all QC results. Currently, there will only be the following files in there
   * `${basename}_fastqc.html`: `fastqc` HTML report
   * `${basename}_fastqc.zip`: Zipped `fastqc` results.
    
   The `basename` is the filename of the FASTQ without the above-mentioned suffixes or suffix combinations. For instance, `fastq_a.fastq.tar.gz` will have the basename `fastq_a`. 
   
  You should not have twice the same filename with different paths in your list of inputs. E.g. the parameter `--input=a/path/file1.fastq,another/path/file1.fastq` will result in an output directory clash, because both files will reduce to `file1.fastq`. The workflow will check the inputs and refuse to start if a clash happens.

### Profiles

Two types of profiles are defined in the `nextflow.config` -- for defining the executor ("local", "lsf") and the execution environment ("test", "conda", "docker", "apptainer"). To run the workflow in an LSF cluster, for instance, you would run Nextflow with

```bash
nextflow run -profile lsf,apptainer ...
```

## Development

  * There is supplementary code in the `utils` directory, including unit-tests, that is used in the `main.nf`. The code is build with `./gradlew jar` and written into `libs/`, where Nextflow will find it. 
  * The `tests/` directory contains sequence data in various formats for the integration tests.


### Building the Docker Container

To run the integration tests with a Docker container containing the current code:

```bash
cd $repoDir
containerVersion="1.2.1"
docker build \
  --rm \
  -t registry.gitlab.com/one-touch-pipeline/workflows/nf-seq-qc:$containerVersion \
  --build-arg "HTTP_PROXY=$HTTP_PROXY" \
  --build-arg "HTTPS_PROXY=$HTTPS_PROXY" \
  -f container-specs/Dockerfile \
  ./
mamba env create -n nf-seq-qc -f test-environment.yml
mamba activate nf-seq-qc
bash integration-tests/run.sh docker test-output/
```
    
### Building the Apptainer Container

The following commands will build the Apptainer container and run the integration tests with it:

```bash
apptainer \
  build \
  cache/apptainer/registry.gitlab.com-one-touch-pipeline-workflows-nf-seq-qc-$containerVersion.img \
  docker-daemon://registry.gitlab.com/one-touch-pipeline/workflows/nf-seq-qc:$containerVersion
```

The name of this image, is the default name chosen by Nextflow, when converting the Docker container into a Apptainer container.

After that, the integration tests should run:

```bash
conda activate nf-seq-qc
bash integration-tests/run.sh apptainer test-output/
```

### Running Unit Tests

```bash
cd $repoDir
./gradlew build
```

## Change Log

* 1.2.1
  * Patch: Migrated Conda to plain Debian container.
  * Patch: Removed process resource settings from `nextflow.config` because these are anyway taken from the `main.nf`.
  * Patch: Replaced `process { ... }` blocks in `nextflow.config` with `process.varName` references. The former do not combine with multiple profiles, but override.
  * Patch: Bumped minimum Nextflow to 23.10.1. Version 22 uses `singularity exec`, while 23 uses `singularity run`, which impacts process isolation.
  * Patch: Use Apptainer instead of Singularity
  * Patch: Change default container name to Nextflow's default for downloading the container.
  * Patch: Updated container image.
  * Patch: Renamed `tests/runIntegrationTests` to `integration-tests/run.sh`.

* 1.2.0
  * Minor: Added support for CRAM input.
  * Patch: Bumped base-container to miniconda3:4.12.0 and simplified `Dockerfile`.

* 1.1.0
  * Minor: Use `-env none` for "lsf" cluster profile. Local environment should not be copied.
  * Patch: Updated `nextflow.config` to fix LSF memory issue with Nextflow. You also need to update to Nextflow >= 22.07.1.
  * Patch: Fixed exponential backoff for memory allocation.
  * Patch: Added unique job names.

* 1.0.0
  * Major: Just [fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) with some code to allow input of various compressed formats.
    
## License & Contributors

See [LICENSE.txt](LICENSE.txt) and [CONTRIBUTORS](CONTRIBUTORS).
