/*
 * Copyright (c) 2021. German Cancer Research Center (DKFZ).
 *
 * Distributed under the MIT License.
 * https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc/-/blob/initial-version/LICENSE.txt
 *
 * Author: WESkit Team & OTP Authors
 */

import otp.nfseqqc.SuffixAnalysisResult
import otp.nfseqqc.Utils

import java.nio.file.Paths

/** Comma-separated list of input FASTQs. */
params.input

/** Output directory. Each input file will get a directory with results in the outputDir. */
params.outputDir = "./"

/** Maximal number of CPUs to use for any process. Required for the CI. */
params.maxCpus = "1"


// Parameter checks
allowedParameters = ['input', 'outputDir', 'maxCpus']

unknownParameters = Utils.collectUnknownParameters(params, allowedParameters)
if (unknownParameters.size() > 0) {
    log.error("There are unrecognized parameters: ${unknownParameters}")
    exit(1)
}

inputs = params.input.split(',') as List<String>
if (inputs.size() == 0) {
    log.error("No input files")
    exit(2)
}

clashingInputs = Utils.collectClashingInputs(inputs)
if (clashingInputs.size() > 0) {
    log.error("There are input files reducing to the same basename: ${clashingInputs}")
    exit(3)
}

// Ensure output directory exists and is write and executable.
outputDir = new File (params.outputDir.toString())
if (!outputDir.exists())
    outputDir.mkdir()

if (!outputDir.isDirectory() || !(outputDir.canExecute() & outputDir.canWrite())) {
    log.error("outputDir is not a directory with write and execute rights: ${outputDir}")
    exit(4)
}

log.info Utils.parameterSummary(
        workflow.manifest.name,
        allowedParameters as List<String>,
        params as LinkedHashMap<String, String>)

/* Type the maxCpus parameter to Integer, for later usage.
   Note that the type of params. maxCpus seems not to be changeable -- at least in some cases.
   Here if params.maxCpus = null, after the re-assignment the type would be Optional, but if
   params.maxCpus = 1 (Integer), the type cannot be changed anymore to Optional.
 */
maxCpus = Optional.
        ofNullable(params.maxCpus).
        map { Integer.parseInt(it.toString()) }


process fastqc {
    tag "fastqc on '$sequenceFile'"

    // Decompression and fastqc
    cpus { Math.min(2, maxCpus.orElse(2)) }
    // The biobambam paper states something like 133 MB.
    memory 260.MB
    time { 4.hours * 2**(task.attempt - 1) }
    maxRetries 2

    publishDir outputDir.toString()

    input:
    file sequenceFile

    output:
    tuple file(fastqcHtmlReport), file(fastqcReportZip)

    shell:
    reportDir = new File(sequenceFile.name + "_reports")
    SuffixAnalysisResult inputInfo = SuffixAnalysisResult.from(sequenceFile.toFile()).get()
    fastqcHtmlReport = new File(reportDir, "${inputInfo.getBasename()}_fastqc.html")
    fastqcReportZip = new File(reportDir, "${inputInfo.getBasename()}_fastqc.zip")
    """
    set -uvex -o pipefail

    ${inputInfo.getArchiveTestCommand()} \
        || echo "Input file pre-test failed': ${inputInfo.getArchiveTestCommand()}"

    export JAVA_OPTS="-Xmx:256m"
    mkdir -p '$reportDir'
    ${inputInfo.getDecompressionCommand()} \
        |  fastqc /dev/stdin \
            --format '${inputInfo.getFastqcFormatName()}' \
            --noextract \
            --nogroup \
            --outdir '$reportDir'

    mv '$reportDir/stdin_fastqc.html' '$fastqcHtmlReport'
    mv '$reportDir/stdin_fastqc.zip'  '$fastqcReportZip'
    """

}

workflow {

    // Data processing
    sequenceFiles_ch = Channel.fromPath(params.input.split(',') as List<String>,
                                        checkIfExists: true)
    fastqc(sequenceFiles_ch)

}

workflow.onComplete {
    println "Workflow run $workflow.runName completed at $workflow.complete with status " +
            "${ workflow.success ? 'success' : 'failure' }"
}
